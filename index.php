<?php
session_start();
require_once("class.user.php");
$login = new USER();

if($login->is_loggedin()!="")
{
	$login->redirect('courses-simple.php');
}

if(isset($_POST['btn-login']))
{
	$uname = strip_tags($_POST['txt_uname_email']);
	$umail = strip_tags($_POST['txt_uname_email']);
	$upass = strip_tags($_POST['txt_password']);
		
	if($login->doLogin($umail,$upass))
	{

        if ($login->isAdmin($umail)) {
            $login->redirect('courses.php');
        }
        else{
		$login->redirect('courses-simple.php');
        }
	}
	else
	{
		$error = "Algún dato es incorrecto, por favor intenta nuevamente";
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Educacont login</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/custom.css" type="text/css"  />
</head>
<body>

<div class="signin-form">

	<div class="container">
     
        
       <form class="form-signin" method="post" id="login-form">
      
        <img src="img/educacont-logo.png" alt="" style="margin-left: 30%;" />
        
        <div id="error">
        <?php
			if(isset($error))
			{
				?>
                <div class="alert alert-danger">
                   <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?> !
                </div>
                <?php
			}
		?>
        </div>
        
        <div class="form-group">
        <input type="text" class="form-control" name="txt_uname_email" placeholder="correo" 
            oninvalid="this.setCustomValidity('Por favor, escribe tu usurio')"
            oninput="setCustomValidity('')" required />
        <span id="check-e"></span>
        </div>
        
        <div class="form-group">
        <input type="password" class="form-control" name="txt_password" placeholder="contraseña"  
            oninvalid="this.setCustomValidity('Por favor, escribe tu contraseña')"
            oninput="setCustomValidity('')" required />
        </div>
       
     	<hr />
        
        <div class="form-group" id="btn-submit">
            <button type="submit" name="btn-login" class="btn btn-default" >
                	<i class="glyphicon glyphicon-log-in"></i> &nbsp; Iniciar sesión
            </button>
        </div>  
      	<br />
            <label>¿No tienes una cuenta?<a href="sign-up.php"> Registrate</a></label><br /><br>
            <label><a href="reset_password.php">¿Olvidaste tu contraseña?</a></label>
      </form>

    </div>
    
</div>

</body>
</html>