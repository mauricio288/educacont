<?php
require ('session_check.php');
require_once ('dbconfig.php');

$find = new USER();
$add = new USER();

$stmt = $find->runQuery("SELECT  course_id , course_name , course_description , course_begin_date , course_price  FROM courses");
$stmt->execute();

if(isset($_POST['save_course']))
{
	$cname = strip_tags($_POST['txt_cname']);
	$cdescription = strip_tags($_POST['txt_cdescription']);
	$cdate = strip_tags($_POST['txt_cdate']);	
	$cprice = strip_tags($_POST['num_cprice']);	
	
	try
	{
		if($find->registerCourse($cname,$cdescription,$cprice,$cdate)){	
			$find->redirect('courses.php');
		}
	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}
}

if(isset($_POST['register_course']))
{

	$courseId = strip_tags($_POST['selected_course_id']);

	$stmt = $find->runQuery("SELECT course_name FROM courses WHERE course_id= '$courseId' ");
	$stmt->execute();
	$courseName=$stmt->fetch(PDO::FETCH_ASSOC);

	$umail = $auth_user->runQuery("SELECT * FROM users WHERE user_id=:user_id");
	$umail->execute(array(":user_id"=>$user_id));
	$userMail=$umail->fetch(PDO::FETCH_ASSOC);

	try{
		// SENDING MAIL TO ADMINISTRATOR
	    $to      = 'pagos@educacont.com.mx';
	    $subject = 'Notificación de inscripción';
	    $message = 'El usuario con correo ' . $userMail['user_email'] . ' desea inscribir el curso ' . $courseName['course_name'];
	    $headers = 'De: Sistema de inscripciones' . "\r\n" .
	        'Reply-To:' . $userMail[user_email] . "\r\n" .
	        'X-Mailer: PHP/' . phpversion();

	    mail($to, $subject, $message, $headers);

	    // WE MAKE A REDIRECT FOR PAGE TO RELOAD THE QUERY
	    $find->redirect('courses-simple.php');
    }
    catch(Exception $e ){
    	echo $e->getMessage();
    }
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
	<script type="text/javascript" src="jquery-1.11.3-jquery.min.js"></script>
	<link rel="stylesheet" href="css/custom.css" type="text/css"  />
	<title>Bienvenido - <?php print($userRow['user_email']); ?></title>
</head>

<body>

	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<a class="pull-left"><img src="img/educacont-logo.png"></a> 
					<li><a for="link" href="courses-simple.php">Cursos</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">

					<li class="dropdown">
						<a for="link" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						<span class="glyphicon glyphicon-user"></span>&nbsp;Hola <?php echo $userRow['user_name']; ?>&nbsp;<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="logout.php?logout=true"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Cerrar sesión</a></li>
						</ul>
					</li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>

	<div class="clearfix"></div>

	<div class="container-fluid" style="margin-top:80px;">

		<div class="container">

			<div class="panel panel-default">
				<!-- Default panel contents -->
				<div class="panel-heading" style="text-align:center" ><b>Cursos actuales</b></div>

				<!-- Table -->
				<table class="table">
					<tr>
						<th>Nombre</th>
						<th>Descripción</th>
						<th>Fecha de inicio</th>
						<th>Costo</th>
						<th></th>
					</tr>

					<?php 
					foreach($stmt->FetchAll() as $results) {						
						echo "<tr>";
						echo '<td>' . $results['course_name'] . '</td>';
						echo '<td>' . $results['course_description'] . '</td>';
						echo '<td>' . $results['course_begin_date'] . '</td>';
						echo '<td class="price">' . "$" . $results['course_price'] . '</td>';
						echo '<form action="" method="post">';
						echo '<td class="inscription">';
						echo '<input type="hidden" name="selected_course_id" value="' . $results['course_id'] . '">';
						echo '<input type="submit" class="btn btn-info" value="Solicitar inscripción" /> ';
						echo ' <input type="hidden" name="register_course" value="1" />';
						echo '</form>';
						echo "</td>";
						echo "</tr>";
							
					}
					?>

				</table>
			</div>
			
		</div>

	</div>

	<script src="bootstrap/js/bootstrap.min.js"></script>

	<script>
		$(".price").each(function( index ) {
			if ($( this ).text()=="$0") {
				$( this ).html("Sin asignar");
			}
		});
	</script>

</body>
</html>