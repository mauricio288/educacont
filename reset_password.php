<?php
session_start();
require_once('class.user.php');
$user = new USER();

if($user->is_loggedin()!="")
{
	$user->redirect('alumns.php');
}

if(isset($_POST['btn-signup']))
{
	
	$umail = strip_tags($_POST['txt_umail']);
	$upass = strip_tags($_POST['txt_upass']);	
	
	if($umail=="")	{
		$error[] = "Escribe un correo";	
	}
	else if(!filter_var($umail, FILTER_VALIDATE_EMAIL))	{
	    $error[] = 'Escribe una dirección de correo valida';
	}
	else if($upass=="")	{
		$error[] = "Escribe una contraseña";
	}
	else if(strlen($upass) < 6){
		$error[] = "La contraseña debe ser minimo de 6 caracteres";	
	}
	else
	{
		try
		{
			$stmt = $user->runQuery("SELECT user_email FROM users WHERE user_email=:umail");
			$stmt->execute(array(':umail'=>$umail));
			$row=$stmt->fetch(PDO::FETCH_ASSOC);
				
			if($user->resetPassword($upass,$umail)){	
				$user->redirect('reset_password.php?updated');
			}
			
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Educacont - Restablacer contraseña</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/custom.css" type="text/css"  />
</head>
<body>

<div class="signin-form">

<div class="container">
    	
        <form method="post" class="form-signin">
            <img src="img/educacont-logo.png" alt="" style="margin-left: 30%;" />
            <br>
            <h4 style="margin-left: 30%;">Recuperar contraseña</h4>
            <?php
			if(isset($error))
			{
			 	foreach($error as $error)
			 	{
					 ?>
                     <div class="alert alert-danger">
                        <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?>
                     </div>
                     <?php
				}
			}
			else if(isset($_GET['updated']))
			{
				 ?>
                 <div class="alert alert-info">
                      <i class="glyphicon glyphicon-log-in"></i> &nbsp; ¡Se ha reestablecido la contraseña! <a href='index.php'>Inicia sesión</a> aquí
                 </div>
                 <?php
			}
			?>

            <div class="form-group">
            <input type="text" class="form-control" name="txt_umail" placeholder="Correo" value="<?php if(isset($error)){echo $umail;}?>" />
            </div>
            <div class="form-group">
            	<input type="password" class="form-control" name="txt_upass" placeholder="Contraseña" />
            </div>
            
            <div class="clearfix"></div><hr />
            <div class="form-group">
            	<button type="submit" class="btn btn-primary" name="btn-signup">
                	<i class="glyphicon glyphicon-open-file"></i>&nbsp;Actualizar contraseña
                </button>
            </div>
            <br />
        </form>
       </div>
</div>

</body>
</html>