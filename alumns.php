<?php
require ('session_check.php');
require_once ('dbconfig.php');
$find = new USER();
$admin = new USER();

$stmt = $find->runQuery("SELECT name, user_name ,first_last_name, user_email , user_course , user_pay FROM users");
$stmt->execute();
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
	<script type="text/javascript" src="jquery-1.11.3-jquery.min.js"></script>
	<link rel="stylesheet" href="css/custom.css" type="text/css"  />
	<title>Bienvenido - <?php print($userRow['user_email']); ?></title>
</head>

<body>

	<?php include 'header.html'; ?>

	<div class="clearfix"></div>

	<div class="container-fluid" style="margin-top:80px;">

		<div class="container">

			<div class="panel panel-default">
				<!-- Default panel contents -->
				<div class="panel-heading" style="text-align:center" ><b>Detalles de alumnos</b></div>

				<!-- Table -->
				<table class="table" id="table_headers">
					<tr>
						<th>Nombre</th>
						<th>Correo</th>
						<th>Curso solicitado</th>
						<th style="text-align:center">Pagado</th>
					</tr>
					
					<?php 
					foreach($stmt->FetchAll() as $results) {
						echo "<tr>";
						echo '<td>' . $results['name'] . " " . $results['first_last_name'] . '</td>';
						echo '<td>' . $results['user_email'] . '</td>';
						echo '<td>' . $results['user_course'] . '</td>';
						echo '<td class="user_pay">' . $results['user_pay'] . '</td>';
						echo "</tr>";
					}
					?>

				</table>
			</div>
		</div>

	</div>

	<script>
		$(".user_pay").each(function( index ) {
			if ($( this ).text()=="0") {
				$( this ).html("No");
			} else if($( this ).text()=="1") {
				$( this ).html("Si");
			}
		});
	</script>

	<script src="bootstrap/js/bootstrap.min.js"></script>

</body>
</html>