<?php
session_start();
require_once('class.user.php');
$user = new USER();

if($user->is_loggedin()!="")
{
	$user->redirect('alumns.php');
}

if(isset($_POST['btn-signup']))
{
	$uname = strip_tags($_POST['txt_uname']);
	$umail = strip_tags($_POST['txt_umail']);
	$upass = strip_tags($_POST['txt_upass']);	
	$rname = strip_tags($_POST['txt_rname']);	
	$ulastname = strip_tags($_POST['txt_ulastname']);	
	
	if($uname=="")	{
		$error[] = "Escribe un nombre de usuario";	
	}
	else if($umail=="")	{
		$error[] = "Escribe un correo";	
	}
	else if(!filter_var($umail, FILTER_VALIDATE_EMAIL))	{
	    $error[] = 'Escribe una dirección de correo valida';
	}
	else if($upass=="")	{
		$error[] = "Escribe una contraseña";
	}
	else if(strlen($upass) < 6){
		$error[] = "La contraseña debe ser minimo de 6 caracteres";	
	}
	else
	{
		try
		{
			$stmt = $user->runQuery("SELECT user_name, user_email FROM users WHERE user_name=:uname OR user_email=:umail");
			$stmt->execute(array(':uname'=>$uname, ':umail'=>$umail));
			$row=$stmt->fetch(PDO::FETCH_ASSOC);
				
			if($row['user_name']==$uname) {
				$error[] = "Lo sentimos, este usuario ya esta usado";
			}
			else if($row['user_email']==$umail) {
				$error[] = "Lo sentimos el correo ya esta registrado";
			}
			else
			{
				if($user->register($uname,$umail,$upass,$rname,$ulastname)){	
				    $user->redirect('sign-up.php?joined');

				    // SENDING MAIL TO 	NEW USER
				    $to      = $umail;
				    $subject = 'Notificación de inscripción';
				    $headers = 'De: Sistema de inscripciones' . "\r\n" .
					$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

					$message = 'Bienvenido a Educacont, para iniciar sesión da clic en el siguiente enlace <br> <a href="http://educacont.com.mx/acceso/"> Acceso educacont </a> ';

				        'X-Mailer: PHP/' . phpversion();

				    mail($to, $subject, $message, $headers);
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Educacont - Registro</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/custom.css" type="text/css"  />
</head>
<body>

<div class="signin-form">

<div class="container">
    	
        <form method="post" class="form-signin">
            <img src="img/educacont-logo.png" alt="" style="margin-left: 30%;" />
            <?php
				if(isset($error))
				{
				 	foreach($error as $error)
				 	{
						 ?>
	                     <div class="alert alert-danger">
	                        <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?>
	                     </div>
	                     <?php
					}
				}
				else if(isset($_GET['joined']))
				{
			?>
             <div class="alert alert-info">
                  <i class="glyphicon glyphicon-log-in"></i> &nbsp; ¡Gracias por registrarte! <a href='index.php'>Inicia sesión</a> aquí
             </div>

            <?php
				}
			?>
            <div class="form-group">
            <input type="text" class="form-control" name="txt_rname" placeholder="Nombre" />
            </div>
            <div class="form-group">
            <input type="text" class="form-control" name="txt_ulastname" placeholder="Apellido" />
            </div>
            <div class="form-group">
            <input type="text" class="form-control" name="txt_uname" placeholder="Nombre de usuario" value="<?php if(isset($error)){echo $uname;}?>" />
            </div>
            <div class="form-group">
            <input type="text" class="form-control" name="txt_umail" placeholder="Correo" value="<?php if(isset($error)){echo $umail;}?>" />
            </div>
            <div class="form-group">
            	<input type="password" class="form-control" name="txt_upass" placeholder="Contraseña" />
            </div>
            
            <div class="clearfix"></div><hr />
            <div class="form-group">
            	<button type="submit" class="btn btn-primary" name="btn-signup">
                	<i class="glyphicon glyphicon-open-file"></i>&nbsp;Registrarse
                </button>
            </div>
            <br />
            <label>¿Tienes ya una cuenta?<a href="index.php"> Inicia sesión</a></label>
        </form>
       </div>
</div>

</div>

</body>
</html>