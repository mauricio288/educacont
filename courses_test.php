<?php
require ('session_check.php');
require_once ('dbconfig.php');
$find = new USER();
$del = new USER();

$stmt = $find->runQuery("SELECT  course_id , course_name , course_description , course_begin_date , course_price  FROM courses");
$stmt->execute();

if(isset($_POST['save_course']))
{
	$cname = strip_tags($_POST['txt_cname']);
	$cdescription = strip_tags($_POST['txt_cdescription']);
	$cdate = strip_tags($_POST['txt_cdate']);	
	$cprice = strip_tags($_POST['num_cprice']);	
	
	try
	{
		if($find->registerCourse($cname,$cdescription,$cprice,$cdate)){	
			$find->redirect('courses.php');
		}
	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}
}

if(isset($_POST['delete_course'])) 
{
	$courseid = strip_tags($_POST['delete_course_id']);

	try 
	{
		if ($del->deleteCourse($courseid)) {
				$del->redirect('courses_test.php');
			}	
	} catch (Exception $e) {
		echo $e->getMessage();
	}

}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
	<script type="text/javascript" src="jquery-1.11.3-jquery.min.js"></script>
	<link rel="stylesheet" href="css/custom.css" type="text/css"  />
	<title>Bienvenido - <?php print($userRow['user_email']); ?></title>
</head>

<body>

	<?php include 'header.html'; ?>

	<div class="clearfix"></div>

	<div class="container-fluid" style="margin-top:80px;">

		<div class="container">

			<div class="panel panel-default">
				<!-- Default panel contents -->
				<div class="panel-heading" style="text-align:center" ><b>Cursos actuales</b></div>

				<!-- Table -->
				<table class="table">
					<tr>
						<th>ID</th>
						<th>Nombre</th>
						<th>Descripción</th>
						<th>Fecha de inicio</th>
						<th>Costo</th>
						<th></th>
					</tr>

					<?php 
					foreach($stmt->FetchAll() as $results) {						
						echo "<tr>";
						echo '<td name="txt_id">' . $results['course_id'] . '</td>';
						echo '<td>' . $results['course_name'] . '</td>';
						echo '<td>' . $results['course_description'] . '</td>';
						echo '<td>' . $results['course_begin_date'] . '</td>';
						echo '<td>' . $results['course_price'] . '</td>';
						echo 
						'<td>' .
							'<form method="post" action="">' .
							    '<input type="hidden" name="delete_course_id" value="' . $results['course_id'] . '">
							 	<button type="submit" name="delete_course" class="btn btn-danger">Borrar</button>
							</form>' .
						'</td>' .
						'</tr>';
					}
					?>

				</table>
			</div>

			<!-- Large modal -->
			<!-- Button trigger modal -->
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
				Agregar curso
			</button>

			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Agregar curso</h4>
						</div>
						<div class="modal-body">
							<form method="post">
								<div class="form-group">
									<input type="text" class="form-control" name="txt_cname" placeholder="Nombre de curso"  />
								</div>
								<div class="form-group">
									<textarea class="form-control" name="txt_cdescription" id="" cols="30" rows="5" placeholder="Descripción del curso"></textarea>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="" class="form-grpup">Fecha de inicio de curso</label>
										</div>
										<div class="form-group">
											<input type="date" name="txt_cdate" class="form-control" />
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="" class="form-grpup">Costo de curso</label>
										</div>
										<div class="form-group">
											<input type="number" name="num_cprice" class="form-control" />
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
								<button type="submit" class="btn btn-primary" name="save_course">Guardar curso</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

	</div>

	<script src="bootstrap/js/bootstrap.min.js"></script>

</body>
</html>