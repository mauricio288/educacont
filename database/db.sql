DROP DATABASE IF EXISTS educacont;
CREATE DATABASE educacont
	DEFAULT CHARACTER SET utf8
	DEFAULT COLLATE utf8_general_ci;
USE educacont;

CREATE TABLE users (
	user_id 		INT				NOT NULL AUTO_INCREMENT,
	user_name		VARCHAR(45)		NOT NULL,
	user_type_id    INT          	NOT NULL,
	user_pass		VARCHAR(255)	NOT NULL,
	user_email		VARCHAR(45)		NOT NULL,
	user_pay		INT 			NOT NULL,
	user_course		VARCHAR(45) 	NOT NULL,
	
	name			VARCHAR(45)		NOT NULL,
	first_last_name	VARCHAR(45)		NOT NULL,

	PRIMARY KEY(user_id)
);

CREATE TABLE courses (
	course_id			INT 			NOT NULL AUTO_INCREMENT,
	course_name 		VARCHAR(45)		NOT NULL,
	course_description 	VARCHAR(250)	NOT NULL,
	course_price	 	INT(5)			NOT NULL,
	course_begin_date	VARCHAR(15)		NOT NULL,

	PRIMARY KEY (course_id)
);
